#include <stdio.h>
#include "funcionA.h"
#include "funcionL.h"
#include "funcionE.h"
#include "funcionX.h"

int main(int argc, char const *argv[]) {

  funcionA();
  funcionL();
  funcionE();
  funcionX();

  return 0;
}
